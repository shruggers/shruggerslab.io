# @sheffieldruby

Presentation site for Sheffield Ruby User Group

## Deployment

This site is deployed by GitLab using their pages pipeline infrastructure.

Changes pushed to this branch are re-deployed to the live site ~5 minutes after the push has completed.

You can view the status of any pages builds at https://gitlab.com/shruggers/shruggers.gitlab.io/builds.
