---
layout: default
permalink: /about/slack/index.html
---

### What on earth is Slack?

Courtesy of [Sheffield Digital](http://sheffield.digital/), we now have a dedicated Slack channel for our members to discuss all things Ruby
and for the organisers to keep you up to date with related events happening in the Sheffield Ruby community.

<div class="slack-button">
  <a href="https://sheffielddigital.slack.com/messages/sheffieldruby" class="button">
    <span class="fa fa-slack"></span> Join the conversation on Slack
  </a>
</div>

#### How do I join in?

Unfortunately, Slack is invite only, so you will need to get in touch with **Sheffield Digital** to request access.

<div class="slack-button">
  <a href="https://sheffield.digital/slack/" class="button">
    <span class="fa fa-key"></span> Request access from Sheffield Digital
  </a>
</div>
