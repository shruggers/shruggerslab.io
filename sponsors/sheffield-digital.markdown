---
layout: default
permalink: /sponsors/sheffield-digital/index.html
---

<img class="sd-background" alt="Sheffield Digital" src="/img/sheffield-digital.png" />

The organisers of ShRUG are very grateful to Sheffield Digital for curating and monitoring the Slack community that we use to organise events and talk to our members.

For more information about Sheffield Digital, please visit their website at [https://sheffield.digital/](https://sheffield.digital/).
