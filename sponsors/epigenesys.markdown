---
layout: default
permalink: /sponsors/epigenesys/index.html
---

![epiGenesys](/img/epigenesys.png)

The organisers of ShRUG are very grateful to epiGenesys for their sponsorship of our group, as well as for the help of their staff in organising and running our events.

For more information about epiGenesys, please visit their website at [https://www.epigenesys.org.uk/](https://www.epigenesys.org.uk/).
