---
layout: default
permalink: /sponsors/old/pusher/
---

### Sessions by Pusher

The organisers of ShRUG are very grateful to Pusher for recording our sessions and making them available for others to watch.

For more information about Sessions by Pusher, please visit their website at [https://pusher.com/sessions](https://pusher.com/sessions).

You can see the sessions that Pusher have recorded for us in the **Recorded Sessions** box.
