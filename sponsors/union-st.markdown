---
layout: default
permalink: /sponsors/union-st/index.html
---

![Union St](/img/union-st.png)

The organisers of ShRUG are very grateful to Union St for providing space to run our events, as well as providing co-working space for our event speakers.

For more information about Union St, please visit their website at [http://www.union-st.org/](http://www.union-st.org/).
