---
layout: meetings
title: February - no meetup this month
excerpt: ShRUG is having a break this month
author: https://twitter.com/jamgregory
categories: meetings
tags: []
date: 2019-02-11
time: "-"
published: true
hide_location: true
disable_meetup: true
---

As mentioned before Christmas, we are now planning to run ShRUG events every other month, which means there won't be a meetup this month.

We are currently working to arrange a speaker for March, so please keep an eye on Twitter and our website for further details.
