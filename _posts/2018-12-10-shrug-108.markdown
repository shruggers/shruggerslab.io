---
layout: meetings
title: ShRUG Christmas Meal
excerpt: Eat, drink, and be merry - it's the ShRUG Christmas Meal
author: https://twitter.com/jamgregory
categories: meetings
tags: [food, drink, chat, shruggers, christmas, festive, meal]
date: 2018-12-13 18:30
time: 6:30pm until late
meetup: 256870266
published: true
location: The Cutlery Works
location_addr:  Neepsend Lane, Sheffield, S3 8AT
location_link: https://www.google.co.uk/maps/place/Cutlery+Works/@53.3929477,-1.4802855,17z/data=!3m1!4b1!4m5!3m4!1s0x4879790fdbe1d701:0x257a769bf84197e3!8m2!3d53.3929445!4d-1.4780968
---

Another year has flown by, and it's time for our annual festive meal.

**_Please note, this will be on a Thursday instead of our usual Monday slot due to venue availability._**

This year, we're going to try out Cutlery Works for food, drink and chat. There are plenty of options to choose one so take a look: [Cutlery Works Website](https://cutleryworks.co.uk/)

We'll be around from 6.30pm and if you're coming to join us, please RSVP so we know to wait for you.

**To get to Cutlery Works**, leaving from Infirmary Road tram stop, head towards Penistone Road and cross Penistone Road at the traffic lights. Continue down Rutland Road (Wickes will be on your right) until you cross the river and arrive at the next set of lights. Turn left on to Neepsend Lane and Cutlery Works is your left-hand side.

On behalf of Sheffield Ruby organising team, we hope you have a lovely break over the Christmas period and have a happy New Year.
