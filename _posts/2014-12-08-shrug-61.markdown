---
layout: meetings
title: Sheffield Ruby Christmas Meal
categories: meetings
time: 19.00 to Late
location: Anchorage Bar
show_link: false
show_addr: false
---

It's that time of the year again. Time to put the talks and workshops to
one side for a month and get together for Sheffield Ruby's annual Christmas meal.

This year we're going to [Anchorage Bar](http://www.anchoragebar.co.uk/) for 7pm for food and drinks.

We'll be booking a table, so be sure to let us know that you're coming
so we have confirmed numbers by responding on the Meetup
page or
[dropping us an email](mailto:hello@shrug.org) or [Tweet
us](http://twitter.com/sheffieldruby).
