---
layout: meetings
title: Sheffield Ruby Summer Social
excerpt: Traditional Summer Social with the community
author: https://twitter.com/b_seven_e
categories: meetings
tags: [ruby, community, chat]
date: 2019-07-17 18:15
time: 6.15 to 9.00pm
published: true
location: Kommune, Sheffield
location_addr: Angel St, Sheffield S3 8LS
location_link: https://goo.gl/maps/sit3xhYjFiK8bZAeA
meetup: 262878059
---

It's time for our summer social, and as mentioned previously on the 3rd Wednesday of the month!

We're checking out [Kommune](http://kommune.co.uk/) from 6.15pm to prolong the summer a bit more. We don't need to book this venue, as given it's size, we should be ok. We'll tweet out the table number when we are there, though!

Looking forward to seeing as many #shruggers as we can!
