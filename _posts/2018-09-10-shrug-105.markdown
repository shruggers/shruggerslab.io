---
layout: meetings
title: Open Source Software, or how I learned to stop worrying and love dealing with grumpy people
excerpt: What's it like to maintain an open source project? Nick tells us about his experience at thoughtbot.
author: https://twitter.com/jamgregory
categories:
  - meetings
  - videos
permalink: /meetings/shrug-105/
tags: [ruby, community, chat]
date: 2018-09-10 18:30
time: 6.30 to 8.00pm
published: true
meetup: 254402034
video: https://www.youtube.com/watch?v=QTcXDQpwlps
---

What's it like to maintain an open source project? How could you get involved in one yourself? How do you know what you're doing is working?

We'll discuss my experience taking over `administrate`, how you can do something similar and discuss some strategies to make maintaining projects easier.

#### About our speaker

Nick is a developer at [thoughtbot](https://thoughtbot.com/), where he builds software in a variety of languages and helps lead projects. Nick maintains [`administrate`](https://github.com/thoughtbot/administrate), [`shoulda`](https://github.com/thoughtbot/shoulda) and [`appraisal`](https://github.com/thoughtbot/appraisal) at thoughtbot, plus a few of his own projects.

##### About thoughtbot

We are thoughtbot. We have worked with hundreds of product teams all over the world, from individual founders who are self-funded, to large multi-national organizations. We have also created our own products and dozens of open source libraries.

#### About our venue

Our meetup will take place at our usual venue, [Union St](http://www.union-st.org).

After the meetup, we'll be heading to [The Devonshire Cat](http://www.devonshirecat.co.uk/) for drinks and a chat.
