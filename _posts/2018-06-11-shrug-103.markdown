---
layout: meetings
title: Measuring Chronic Pain Outcomes with Ruby and Twilio
excerpt: Use tech to learn about pain management. We have 'superpowers' to make lives better.
author: https://twitter.com/b_seven_e
categories:
  - meetings
  - videos
permalink: /meetings/shrug-103/
tags: [ruby, twilio, healthtech, metrics]
date: 2018-06-11 18:30
published: true
meetup: 251200153
video: https://www.youtube.com/watch?v=ZtBckwQaEBA
---

We can use our skills to improve the lives of those around us, not just our employers and customers. In 2017 Nick's fiancée's surgeon told her that she would require a hip replacement. The surgeon remarked that barometric pressure very much affected day to day chronic pain. He decided to test this.

Using a Twilio SMS 'front end', Ruby on Rails 'back end', and WeatherUnderground integration Nick promptly built an application to randomly request her pain levels against local barometric pressure for four months and processed the results. The processing involved his first foray into machine learning concepts with Ruby. This talk will tell their story, walk through the exact code as it was executed, and elaborate on our responsibility to 'give back' to those around us with our Ruby 'superpower'.

#### About our speaker

[Nick Schwaderer](https://twitter.com/Schwad4HD14) is a Rails developer for OHQ in Cornwall. He grew up in Montana in the United States, studied at Plymouth University, and served two terms in the State Congress before moving back to the UK and working in software. Beyond the talk project, he has built Ruby projects to improve access to government, encourage personal finance literacy, track militarized equipment used in law enforcement and assist Plymouth students in finding study areas.

#### About our sponsors

We're grateful for [NHS Digital](https://digital.nhs.uk/)'s sponsorship, which has helped Nick travel to talk to us, and for [epiGenesys](https://www.epigenesys.org.uk/)'s sponsorship of the pre-meetup food and drink.

#### About our venue

Our meetup will take place at our usual venue, [Union St](http://www.union-st.org).

After the meetup, we'll be heading to [The Devonshire Cat](http://www.devonshirecat.co.uk/) for drinks and a chat.

##### Pre-meetup food and drink

For this event, we're providing pizza and drinks before the meetup (from around 6pm) at our venue.

If you'd like to attend that, please make sure you RSVP on Meetup and let us know if you have any dietary requirements.
