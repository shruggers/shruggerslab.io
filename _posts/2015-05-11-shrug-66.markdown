---
layout: meetings
title: Pragmatic BDD with ShouldIT?
categories: meetings
time: 6.30 to 8.00pm
meetup: 222252220
published: true
location: University of Sheffield Enterprise
location_addr: 210 Portobello, S1 4AE
location_link: https://maps.google.co.uk/maps?cid=3111695280195828660
---

### What on earth is ShouldIT?

Have you found that as a Ruby developer that you have picked up RSpec or Test Unit with relative ease, yet find the Cucumber too abstract, bulky or even unnecessary? ShouldIT? acts as a bridge between your spec based tests and feature files that can be written clearly and simply using Markdown. Richard will show you how pain free and simple this process can be, making collaboration around software development clearer and more visible for everyone.

### About our speaker

Richard McIntyre is Ruby developer and agile testing coach who is currently contracted to Sky Technology in Leeds.

Paul Lemon, head of Sky Service Digital team, will be introducing Richard's talk, telling us a little bit about Sky's digital centre over in Leeds, and how they're implementing Spotify tribes and squads in their agile teams.

### Tonight's meeting is sponsored by Sky Technology

Sky Technology are creating a digital centre of excellence in Leeds. With a team of 40 on the ground already we a building a team of 300 over the next year. We have a culture of engineer excellence working in small highly collaborative and agile teams. We are recruiting the full range of software roles including engineers, scrum masters, analysts and testers using technologies including Ruby, Javascript and Java.

* Interested? [Find out more at **workforsky.com**](http://www.workforsky.com/)
