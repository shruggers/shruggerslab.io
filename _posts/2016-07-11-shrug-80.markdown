---
layout: meetings
title: Ruby on Docker and AWS with Convox
excerpt: Let's make Ruby app deployments easier and scalable
author: https://twitter.com/b_seven_e
categories: meetings
tags: [docker, ruby, aws, covox, open source]
date: 2016-07-11 18:30
time: 6.30 to 8.30pm
meetup: 231534760
published: true
---

### Ruby on Docker and AWS with Convox

Luke recently started working for [Convox](http://convox.com/) which is an open source, private platform as a service (PaaS) written predominantly in Go. Years of experience coupled with the testing of a large open source community and access to a huge library of pre-built images makes using Convox a robust and reliable yet developer-friendly experience. He will discuss how it works, what it’s like developing in the open, how you can get involved and be doing a short demo.

He also invites you to bring along any dockerized projects and he would be happy to help you test it out for yourself (potentially in the pub)!

#### About our speaker

[Luke Roberts](https://twitter.com/awsmsrc) is a full-stack developer currently based in San Francisco but will be (re)based in Sheffield by the time he will give this talk. He is also a veteran when it comes to speaking at #shrug.

He has been working on various backend applications for over 5 years now and loves working remotely and on open source projects. He also loves guitars and robots (but who doesn’t).

#### About our venue

Our meetup tonight will take place at our venue, [Union St](http://www.union-st.org).

After the meetup, we'll be heading to [The Devonshire Cat](http://www.devonshirecat.co.uk/) for drinks, food and chat.
