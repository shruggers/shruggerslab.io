---
layout: meetings
title: On writing &lt;&#37;&#61; code &#37;&gt;
excerpt: What can 5000 years of written language teach us about writing code?
author: https://twitter.com/jamgregory
categories: meetings
tags: [ruby newby, ruby, shruggers, community, outreach]
date: 2018-03-12 18:30
published: true
meetup: 248478297
---

Writing code in high-level programming languages began in the 1950s.

In contrast, writing goes back more than 5000 years. So, what can we learn from people who study writing?

This talk explores ideas from writing experts that can help us write code well.

#### About our speaker

[Elliott](https://twitter.com/elliotthilaire) is a Ruby and Elixir developer from Brisbane, Australia.

He works at Square Enix in London and likes cycling, nature photography and travelling.

#### About our venue

Our meetup will take place at our venue, [Union St](http://www.union-st.org).

After the meetup, we'll be heading to [The Devonshire Cat](http://www.devonshirecat.co.uk/) for drinks, food and chat.
