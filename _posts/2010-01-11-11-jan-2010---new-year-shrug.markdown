---
layout: default
categories: meetings
title: New Year ShRUG
hide_location: true
permalink: /meetings/shrug-2/
---

The second meeting of ShRUG is another informal pub meeting. We're trying out a new venue, Mojo.

We'll be arriving at 6.30pm to start talking Ruby (and related stuff) at 7pm.

## Where to find us

Mojo is near Waitrose, off London Road. It's at the old Dulo venue.
